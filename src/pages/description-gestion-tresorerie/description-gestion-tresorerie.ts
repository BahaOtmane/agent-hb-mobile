import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PackGestionTresoreriePage} from "../pack-gestion-tresorerie/pack-gestion-tresorerie";

@IonicPage()
@Component({
  selector: 'page-description-gestion-tresorerie',
  templateUrl: 'description-gestion-tresorerie.html',
})
export class DescriptionGestionTresoreriePage {
  profil:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.profil=JSON.parse(localStorage.getItem("profil"));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DescriptionGestionTresoreriePage');
  }

  back(){
    this.navCtrl.pop();
  }

  getPack(){
    if(this.profil == 'Particulier' || this.profil == 'Autre'){
      window.open("http://www.agenthb.com/devis");
    }
    else{
      this.navCtrl.push(PackGestionTresoreriePage);
    }
  }

}
