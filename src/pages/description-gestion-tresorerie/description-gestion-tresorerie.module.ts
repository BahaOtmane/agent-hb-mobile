import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DescriptionGestionTresoreriePage } from './description-gestion-tresorerie';

@NgModule({
  declarations: [
    DescriptionGestionTresoreriePage,
  ],
  imports: [
    IonicPageModule.forChild(DescriptionGestionTresoreriePage),
  ],
})
export class DescriptionGestionTresoreriePageModule {}
