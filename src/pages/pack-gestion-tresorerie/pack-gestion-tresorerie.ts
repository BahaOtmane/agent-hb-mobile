import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-pack-gestion-tresorerie',
  templateUrl: 'pack-gestion-tresorerie.html',
})
export class PackGestionTresoreriePage {
  profil:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.profil=JSON.parse(localStorage.getItem("profil"));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PackGestionTresoreriePage');
  }

  back(){
    this.navCtrl.pop();
  }

  getService(){
    window.open("http://www.agenthb.com/tarifs/selection/2", '_system');
  }
}
