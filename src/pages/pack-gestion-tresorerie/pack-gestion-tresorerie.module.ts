import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PackGestionTresoreriePage } from './pack-gestion-tresorerie';

@NgModule({
  declarations: [
    PackGestionTresoreriePage,
  ],
  imports: [
    IonicPageModule.forChild(PackGestionTresoreriePage),
  ],
})
export class PackGestionTresoreriePageModule {}
