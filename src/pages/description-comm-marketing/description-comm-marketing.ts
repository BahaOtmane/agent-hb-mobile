import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PackCommMarketingPage} from "../pack-comm-marketing/pack-comm-marketing";

@IonicPage()
@Component({
  selector: 'page-description-comm-marketing',
  templateUrl: 'description-comm-marketing.html',
})
export class DescriptionCommMarketingPage {
  profil:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.profil=JSON.parse(localStorage.getItem("profil"));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DescriptionCommMarketingPage');
  }

  back(){
    this.navCtrl.pop();
  }

  getPack(){
    if(this.profil == 'Particulier' || this.profil == 'Autre'){
      window.open("http://www.agenthb.com/devis");
    }
    else{
      this.navCtrl.push(PackCommMarketingPage);
    }
  }
}
