import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DescriptionCommMarketingPage } from './description-comm-marketing';

@NgModule({
  declarations: [
    DescriptionCommMarketingPage,
  ],
  imports: [
    IonicPageModule.forChild(DescriptionCommMarketingPage),
  ],
})
export class DescriptionCommMarketingPageModule {}
