import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PackGestionAdminPage} from "../pack-gestion-admin/pack-gestion-admin";

@IonicPage()
@Component({
  selector: 'page-description-gestion-admin',
  templateUrl: 'description-gestion-admin.html',
})
export class DescriptionGestionAdminPage {
  profil:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.profil=JSON.parse(localStorage.getItem("profil"));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DescriptionGestionAdminPage');
  }

  back(){
    this.navCtrl.pop();
  }

  getPack(){
    if(this.profil == 'Particulier' || this.profil == 'Autre'){
      window.open("http://www.agenthb.com/devis");
    }
    else{
      this.navCtrl.push(PackGestionAdminPage);
    }
  }

}
