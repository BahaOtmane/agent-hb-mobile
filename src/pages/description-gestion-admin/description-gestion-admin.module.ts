import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DescriptionGestionAdminPage } from './description-gestion-admin';

@NgModule({
  declarations: [
    DescriptionGestionAdminPage,
  ],
  imports: [
    IonicPageModule.forChild(DescriptionGestionAdminPage),
  ],
})
export class DescriptionGestionAdminPageModule {}
