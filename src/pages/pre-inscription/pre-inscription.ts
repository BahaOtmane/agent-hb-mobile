import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
//import {InscriptionPage} from "../inscription/inscription";
import {Facebook} from "@ionic-native/facebook";
import {InscriptionPage} from "../inscription/inscription";
import {GooglePlus} from "@ionic-native/google-plus";

@IonicPage()
@Component({
  selector: 'page-pre-inscription',
  templateUrl: 'pre-inscription.html',
})
export class PreInscriptionPage {
  isLoggedIn:boolean = false;
  userFB: any;
  userGP:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private fb:Facebook, private googlePlus:GooglePlus) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PreInscriptionPage');
  }

  inscrire(){
    this.navCtrl.push(InscriptionPage);
  }

  getUserDetail(userid) {
    this.fb.api("/"+userid+"/?fields=id,email,name,picture,gender",["public_profile"])
      .then(res => {
        this.userFB = res;
        this.navCtrl.push(InscriptionPage,{userFB:this.userFB});
      })
      .catch(e => {
        console.log(e);
      });
  }
  inscrireAvecFacebook(){
    this.fb.login(['public_profile', 'user_friends', 'email'])
      .then(res => {
        if(res.status === "connected") {
          this.isLoggedIn = true;
          this.getUserDetail(res.authResponse.userID);
        } else {
          this.isLoggedIn = false;
        }
      })
      .catch(e => console.log('Error logging into Facebook', e));
  }


  inscrireAvecGoogle(){
    this.googlePlus.login({})
      .then(res => {
        console.log(res);
        this.userGP = res;
        this.isLoggedIn = true;
        this.navCtrl.push(InscriptionPage,{userGP:this.userGP});
      })
      .catch(err => console.error(err));
  }

}
