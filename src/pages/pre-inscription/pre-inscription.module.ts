import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PreInscriptionPage } from './pre-inscription';

@NgModule({
  declarations: [
    PreInscriptionPage,
  ],
  imports: [
    IonicPageModule.forChild(PreInscriptionPage),
  ],
})
export class PreInscriptionPageModule {}
