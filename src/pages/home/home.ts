import { Component } from '@angular/core';
import {LoadingController, NavController} from 'ionic-angular';
import {ServicesPage} from "../services/services";
//import {InscriptionPage} from "../inscription/inscription";
import {PreInscriptionPage} from "../pre-inscription/pre-inscription";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public local:Storage;

  constructor(public navCtrl: NavController, public loadingCtrl:LoadingController) {
    this.local=localStorage;
  }

  ionViewCanEnter(){
    let loading=this.loadingCtrl.create({
      content:"Chargement..."
    });
    if (this.local.length > 1) {
      loading.present();
      this.navCtrl.push(ServicesPage);
      loading.dismiss();
    }
    else{
      loading.present();
      this.navCtrl.push(PreInscriptionPage);
      loading.dismiss();
    }
  }
}
