import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PackCommMarketingPage } from './pack-comm-marketing';

@NgModule({
  declarations: [
    PackCommMarketingPage,
  ],
  imports: [
    IonicPageModule.forChild(PackCommMarketingPage),
  ],
})
export class PackCommMarketingPageModule {}
