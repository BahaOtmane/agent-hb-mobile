import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ServicesPage} from "../services/services";
import {Information} from "../../model/information.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@IonicPage()
@Component({
  selector: 'page-inscription',
  templateUrl: 'inscription.html',
})
export class InscriptionPage {
  information:Information=new Information();
  userFB:any;
  userGP:any;
  public local:Storage;
  private formgrp : FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder) {
    this.userFB=this.navParams.data.userFB;
    this.userGP=this.navParams.data.userGP;
    this.local=localStorage;
    this.formgrp = this.formBuilder.group({
      prenom: ['', Validators.required],
      nom: ['', Validators.required],
      telephone: ['', Validators.required],
      mail: ['', Validators.required],
      profil: ['', Validators.required],
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InscriptionPage');
  }

  setInformations(information){
    this.local.setItem('prenom',JSON.stringify(information.prenom));
    this.local.setItem('nom',JSON.stringify(information.nom));
    this.local.setItem('telephone',JSON.stringify(information.telephone));
    this.local.setItem('mail',JSON.stringify(information.mail));
    this.local.setItem('profil',JSON.stringify(information.profil));
    this.navCtrl.push(ServicesPage);
  }

}
