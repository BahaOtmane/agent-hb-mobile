import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {DescriptionGestionAdminPage} from "../description-gestion-admin/description-gestion-admin";
import {DescriptionGestionTresoreriePage} from "../description-gestion-tresorerie/description-gestion-tresorerie";
import {DescriptionCommMarketingPage} from "../description-comm-marketing/description-comm-marketing";

@IonicPage()
@Component({
  selector: 'page-services',
  templateUrl: 'services.html',
})
export class ServicesPage {
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ServicesPage');
  }

  packGestionAdmin(){
    this.navCtrl.push(DescriptionGestionAdminPage);
  }
  packGestionTres(){
    this.navCtrl.push(DescriptionGestionTresoreriePage);
  }
  packCommMarke(){
    this.navCtrl.push(DescriptionCommMarketingPage);
  }
}
