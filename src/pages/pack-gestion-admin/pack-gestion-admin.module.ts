import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PackGestionAdminPage } from './pack-gestion-admin';

@NgModule({
  declarations: [
    PackGestionAdminPage,
  ],
  imports: [
    IonicPageModule.forChild(PackGestionAdminPage),
  ],
})
export class PackGestionAdminPageModule {}
