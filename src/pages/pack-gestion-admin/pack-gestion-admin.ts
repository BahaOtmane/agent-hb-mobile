import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-pack-gestion-admin',
  templateUrl: 'pack-gestion-admin.html',
})
export class PackGestionAdminPage {
  profil:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.profil=JSON.parse(localStorage.getItem("profil"));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PackGestionAdminPage');
  }

  back(){
    this.navCtrl.pop();
  }

  getService(){
    window.open("http://www.agenthb.com/tarifs/selection/1", '_system');
  }
}
