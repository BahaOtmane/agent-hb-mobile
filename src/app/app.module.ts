import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import {InscriptionPage} from "../pages/inscription/inscription";
import {ServicesPage} from "../pages/services/services";
import {PackCommMarketingPage} from "../pages/pack-comm-marketing/pack-comm-marketing";
import {PackGestionAdminPage} from "../pages/pack-gestion-admin/pack-gestion-admin";
import {PackGestionTresoreriePage} from "../pages/pack-gestion-tresorerie/pack-gestion-tresorerie";
import {DescriptionGestionAdminPage} from "../pages/description-gestion-admin/description-gestion-admin";
import {DescriptionCommMarketingPage} from "../pages/description-comm-marketing/description-comm-marketing";
import {DescriptionGestionTresoreriePage} from "../pages/description-gestion-tresorerie/description-gestion-tresorerie";
import {Facebook} from "@ionic-native/facebook";
import {GooglePlus} from "@ionic-native/google-plus";
import {PreInscriptionPage} from "../pages/pre-inscription/pre-inscription";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    InscriptionPage,
    ServicesPage,
    PackCommMarketingPage,
    PackGestionAdminPage,
    PackGestionTresoreriePage,
    DescriptionGestionAdminPage,
    DescriptionCommMarketingPage,
    DescriptionGestionTresoreriePage,
    PreInscriptionPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    InscriptionPage,
    ServicesPage,
    PackCommMarketingPage,
    PackGestionAdminPage,
    PackGestionTresoreriePage,
    DescriptionGestionAdminPage,
    DescriptionCommMarketingPage,
    DescriptionGestionTresoreriePage,
    PreInscriptionPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Facebook,
    GooglePlus,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
